#!/bin/bash

cp composer.phar /usr/local/bin/composer
cp drush.phar /usr/local/bin/drush
cp drupal.phar /usr/local/bin/drupal
